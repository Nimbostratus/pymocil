#!/usr/bin/env python

# python implementation of teamocil (https://github.com/remiprev/teamocil)
# yes, it's intentionally all in one file, to keep it easily in a bin
# directory.
# Maybe some day this will get a setup.py


# different behavior compared to teamocil:
# - -h maps to help, not --here (not documented in teamocil, but works)
# - optparse-compatible options
# - no support for ERB
# - -c option to show generated commands
# - -t option to set teamocils paths and env vars instead of pymocils

from __future__ import print_function
import os
import sys
import optparse
import subprocess

try:
    import yaml
except ImportError:
    print("pyyaml missing; try aptitude install python-yaml or pip install pyyaml",
          file=sys.stderr)
    sys.exit(1)


class Layout(object):
    def __init__(self, opts, yaml_data):
        self.yaml = yaml_data
        self.opts = opts
        if not "windows" in self.yaml:
            print("the layout file's root node has to be windows or session",
                  file=sys.stderr)
            sys.exit(5)

    def add_window_commands(self, window, index, commands):
        name = "name" in window and window["name"] or "pymocil-window-%s" % (index + 1)
        if self.opts.here and index == 0:
            commands.append("tmux rename-window '%s'" % name)
        else:
            commands.append("tmux new-window -n '%s'" % name)

        select_pane = 0
        if "splits" in window:
            for i, s in enumerate(window["splits"]):
                if "focus" in s and s["focus"]:
                    select_pane = i
                self.add_split_commands(window, s, i, commands)

        if "options" in window:
            for k, v in window["options"].items():
                commands.append("tmux set-window-option %s %s" % (k, v and "on" or "off"))

        commands.append("tmux select-pane -t %s" % select_pane)

    def add_split_commands(self, window, split, index, commands):

        def _setfilter(src, cmd):
            if isinstance(src, list):
                for f in src:
                    cmd.append(f)
            else:
                cmd.append(src)

        if index > 0:
            target = "target" in split and "-t %s" % split["target"] or ""
            if "width" in split:
                commands.append("tmux split-window -h -p %s %s" % (split["width"], target))
            elif "height" in split:
                commands.append("tmux split-window -p %s %s" % (split["height"], target))
            else:
                commands.append("tmux split-window %s" % target)

        cmd = []

        try:
            _setfilter(window["filters"]["before"], cmd)
        except KeyError:
            pass

        if "root" in window:
            cmd.append("cd '%s'" % os.path.expanduser(window["root"]))

        if self.opts.teamocilmode:
            cmd.append("export TEAMOCIL=1")
        else:
            cmd.append("export PYMOCIL=1")

        cmd.append(window.get("clear", False) and "clear" or "")
        command = split.get("cmd", None)
        if command is not None:
            if isinstance(command, list):
                for c in command:
                    cmd.append(c)
            else:
                cmd.append(command)

        try:
            _setfilter(window["filters"]["after"], cmd)
        except KeyError:
            pass

        commands.append('tmux send-keys -t %s "%s"' % (index, " && ".join([c for c in cmd if c])))
        commands.append('tmux send-keys -t %s Enter' % index)

    def execute(self):
        # pretty straightforward, and not nicely seperated
        # into objects, but it's not too complex
        commands = []

        session_name = "pymocil-session"
        if self.opts.session_name is not None:
            session_name = self.opts.session_name

        for i, w in enumerate(self.yaml["windows"]):
            self.add_window_commands(w, i, commands)

        if self.opts.showcommands:
            for c in commands:
                print(c)

        # check for tmux environment variables
        # and start tmux first if necessary
        # the attach have to be the last command
        if 'TMUX' in os.environ:
            commands.append("tmux rename-session '%s'" % self.yaml.get("name", session_name))
        else:
            commands.insert(0, "tmux new-session -d -s '%s'" % self.yaml.get("name", session_name))
            commands.append("tmux attach-session")

        for c in commands:
            subprocess.call(c, shell=True)


class PyMocil(object):

    def __init__(self):
        """ constructor - mostly options, paths, modes setup """
        parser = optparse.OptionParser()
        parser.add_option(
            "--here",
            help="open the session in the current window",
            metavar="LAYOUT")
        parser.add_option(
            "-l", "--layout",
            help="use the layout file",
            metavar="FILE")
        parser.add_option(
            "-n", "--session-name",
            dest="session_name",
            help="edit the layout with $EDITOR",
            metavar="SESSIONNAME")
        parser.add_option(
            "-e", "--edit",
            help="edit the layout with $EDITOR",
            metavar="LAYOUT")
        parser.add_option(
            "--list",
            help="list available layouts",
            action="store_true")
        parser.add_option(
            "-s", "--show",
            help="show the layout, do not execute anything",
            metavar="LAYOUT")
        parser.add_option(
            "-c", "--show-commands",
            dest="showcommands",
            help="show the commands the layout executes",
            action="store_true")
        parser.add_option(
            "-t", "--teamocil-mode",
            dest="teamocilmode",
            help="use teamocils names like $TEAMOCIL_PATH or .teamocil instead of .pymocil",
            action="store_true")
        (self.opts, self.args) = parser.parse_args()

        if self.opts.teamocilmode:
            env = os.environ.get("TEAMOCIL_PATH", None)
            default = os.path.expanduser("~/.teamocil")
        else:
            env = os.environ.get("PYMOCIL_PATH", None)
            default = os.path.expanduser("~/.pymocil")
        self.search_path = env or default
        if not os.access(self.search_path, os.R_OK):
            print(
                "layout path %s not accessible, creating" % self.search_path,
                file=sys.stderr)
            os.makedirs(self.search_path)

    def get_layout_file(self, layout, must_exist=False):
        if isinstance(layout, list):
            try:
                layout = layout.pop()
            except IndexError:
                print("no layout to apply", file=sys.stderr)
                sys.exit(3)

        if self.opts.layout is not None:
            filename = "%s.yml" % self.opts.layout
        else:
            filename = os.path.join(self.search_path, "%s.yml" % layout)

        if must_exist and not os.access(filename, os.R_OK):
            print("file %s does not exist" % filename, file=sys.stderr)
            sys.exit(4)

        return filename

    def do_list(self):
        print(" ".join([".".join(f.split(".")[:-1]) for f in os.listdir(self.search_path)]))

    def do_edit(self):
        fname = self.get_layout_file(self.opts.edit, must_exist=False)
        subprocess.call([os.environ.get("EDITOR", "vi"), fname])

    def do_show(self):
        fname = self.get_layout_file(self.opts.show, must_exist=True)
        with open(fname, "r") as l:
            print(l.read())

    def do_apply(self):
        fname = self.get_layout_file(self.args, must_exist=True)
        with open(fname, "r") as f:
            layout = yaml.load(f.read())
        # use the session values if a session key exists
        Layout(self.opts, layout.get("session", layout)).execute()

    def run(self):
        if self.opts.edit:
            self.do_edit()
        elif self.opts.list:
            self.do_list()
        elif self.opts.show:
            self.do_show()
        else:
            self.do_apply()

if __name__ == "__main__":
    p = PyMocil()
    p.run()
