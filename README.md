# Pymocil

Pymocil is a just for fun python clone of the great tool [teamocil](https://github.com/remiprev/teamocil). Its purpose is the creation of tmux sessions from YAML files.

All teamocil YAML files should be understood by Pymocil, though it doesn't claim to be bug free ;)

## Installtion and usage

Pymocil is one single file which may be placed anywhere. Its only dependencies are python and python-yaml, which can be installed via pip or package manager. 

```bash
$ wget https://bitbucket.org/nimbostratus/pymocil/raw/tip/pymocil.py
$ sudo pip install PyYAML
$ tmux
$ python pymocil.py --help
$ python pymocil.py -e test
$ python pymocil.py test
```

## Further reading and thanks

Pymocil implements every current feature of teamocil, so you may take their documentation as a reference for the YAML. The command line option summary can be read by running ./pymocil.py --help.

The teamocil project page and documentation can be found at [github](https://github.com/remiprev/teamocil/blob/master/README.md)

Thanks to teamocil guys for the great tool.

## License

Teamocil is © 2011-2012 [Rémi Prévost](http://exomel.com) and may be freely distributed under the [MIT license](https://github.com/remiprev/teamocil/blob/master/LICENSE).

Pymocil is © 2012 [Sven Bröckling](https://bitbucket.org/nimbostratus) and may be freely distributed under the [MIT license](https://bitbucket.org/nimbostratus/pymocil/raw/tip/LICENSE.txt).

